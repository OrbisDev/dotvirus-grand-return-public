@echo off 
@title Batch Music Player for dotVirus Grand Return
:: if statements go here, program later.
MODE CON: COLS=60 LINES=8

tasklist /FI "IMAGENAME eq wscript.exe" 2>NUL | find /I /N "wscript.exe">NUL
if %ERRORLEVEL%==0 taskkill /IM wscript.exe /f /t>nul 

if %musicneeded% == 1 set musicvar=MUSIC/silenceawait.mp3
if %musicneeded% == 2 set musicvar=MUSIC/mevnavinbound.mp3

set file=%musicvar%
( echo Set Sound = CreateObject("WMPlayer.OCX.7"^)
  echo Sound.URL = "%file%"
  echo Sound.Controls.play
  echo do while Sound.currentmedia.duration = 0
  echo wscript.sleep 100
  echo loop
  echo wscript.sleep (int(Sound.currentmedia.duration^)+1^)*1000) >music.vbs
start /min music.vbs
cls 
echo Unfortunately, due to the way Batch works, this is the best way I can
echo get music into the game without having to resort to another
echo third-party resource. When you're finished playing dotVirus,
echo please press any key to continue.
echo Playing %musicneeded%
echo.
pause
del music.vbs >nul
taskkill /IM wscript.exe /f /t >nul
exit 
