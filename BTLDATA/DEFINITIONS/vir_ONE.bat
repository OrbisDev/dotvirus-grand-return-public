set virdef=Creeper System
longdef=The Creeper System was an ancient virus that originated around 1970. It has the ability to replicate itself, although its code is so ancient it cannot replicate itself on dotOS. 
set resist=2
set decrypt=1
set encrypt=0
set syscode=25

:: Layout is subject to change.
:: Its ability to be compatible with other versions of 
:: DVGR depends solely on how this file structures variables.
:: File created on version: 1.0_base
:: File updated on version: 1.0_base

