@echo off
@set version=1.0_base
@title=dotVirus: Grand Return v%version%
@set debug=1
@set musicneeded=1
@start player.bat


:: Linux users!
:: If you're using WINE, you will see what is saved to a .ini file.
:: Don't fret, there's nothing in there to harm your system.
:: Thanks!
 
:: In the meantime, I need to check if your DVGR client has that 
:: CALLUPONS folder. 

if not exist "CALLUPONS" (
echo DVGR REQUIRES that CALLUPONS folder. If you deleted it by accident, please please please redownload. 
echo For your protection, I've disabled the game until you do. Sorry about that guys.
pause
exit
)

if %debug% == 1 goto debuginsert
:: Welcome to an old hobby. 
ping localhost -n 5 >nul
call "CALLUPONS\spacer.bat"
echo A game written originally by Shubshub
echo.
echo.
ping localhost -n 5 >nul
cls 
call "CALLUPONS\spacer.bat"
echo Reprogrammed and Reimagined by OrbisDev
echo.
echo. 
ping localhost -n 5 >nul
cls
call "CALLUPONS\spacer.bat"
echo  Welcome to dotVirus: Grand Return
echo.
echo.
ping locahost -n 5 >nul

:titleScreen
cls
call "CALLUPONS\spacer.bat"
echo  Welcome to dotVirus: Grand Return
echo.
echo Please enter your inputs on the digital terminal
echo provided for your convenience.
echo.
echo 1) Boot dvgr.sys
echo 2) Load savestate
echo 3) Update terminal
echo 4) dotVirus Credits
echo 5) Close Terminal
if %debug% == 1 echo TERMINAL DEBUG IS NOW ACTIVE
echo.
set /p bootload=:: 
if %bootload% == 1 goto bootdvgr
if %bootload% == 2 goto loadsavestate
if %bootload% == 3 goto updateTerm
if %bootload% == 4 goto dotOSCredits
if %bootload% == 5 goto fancyShutdown
goto titleScreen

:dotOSCredits
cls 
call "CALLUPONS\spacer.bat"
echo Music is by cdk. He makes awesome music.
echo.
echo I'm all coded out for the night. 
echo So I'll just leave some random 
echo numbers for you to stare at until you 
echo get bored of this screen. 
echo.
echo %random%%random%%random% numbers numbers. %random%.
pause 
goto titleScreen




:bootdvgr

:: gotta set settings before we can continue. 

set decoderlvl=1
set encryptorlvl=1
set containlvl=1
set dotOSHP=30
set syscodelvl=1

set statcount=4

cls
echo dotOS is now booting... please wait.
ping localhost -n 7 >nul 
echo.
for /L %%i in (0,4,1024) do (
cls
echo Loaded 00%%i tB of 001024 tB
PING 1.1.1.1 -n 1 -w 1000 >NUL
)
ping localhost -n 3 >nul 
echo System OK... checking memory
ping localhost -n 2 >nul 
for /L %%o in (0,32,8192) do (
cls
echo Loaded 001024 tB of 001024 tB
echo System OK... checking memory
echo 0%%o pB OK of 16384 pB
ping 1.1.1.1 -n 1 -w 1000 >nul
)
echo ERROR SYSTEM FAULT DETECTED
echo MEM. MISMATCH
echo. 
ping localhost -n 3 >nul 
echo Launching EMER. Mode
ping localhost -n 2 >nul
for /L %%p in (0,1,100) do (
SET /a RANval=%RANDOM% * 4 / 32768 + 1
cls
echo %%p BITS LOADED
ping localhost -n %RANval% >nul
)
cls 
echo Welcome to the dotOS EMERCenter v.%version%.
echo.
echo Firstly, we must create the EMERCenter Hyper-User.
echo To begin, we need your desired username.
echo. 
set /p hyperu=What is your desired username:: 
echo.
echo Welcome %hyperu%. The EMERCenter will guide you through
echo the tutorial on restoring your precious dotOS! 
echo. 
echo We will be loading the tutorial now. 
pause 
cls
for /L %%e in (0,4,100) do (
echo %%e %%e %%e %%e %%e %%e %%e %%e %%e %%e %%e %%e loaded.
ping localhost -n 1 >nul 
)

echo Hello and welcome to the EMERCenter tutorial center! 
echo Here, we will cultivate your skills in fixing your
echo dotOS (and many more to come!) 
echo. 
echo We will, however need to punch in some simple variables
echo to help you get along!
ping localhost -n 10 >nul 
echo Unfortunately. Your dotOS can only handle 4 versions! 
echo Please, put them to your liking. 
echo. 

:debuginsert
if %debug% == 1 echo TERMINAL DEBUG
if %debug% == 1 set statcount=4
if %debug% == 1 set decoderlvl=1
if %debug% == 1 set encryptorlvl=1
if %debug% == 1 set containlvl=1
if %debug% == 1 set dotOSHP=30
if %debug% == 1 set syscodelvl=1
if %debug% == 1 set hyperu=DEBUG_%RANDOM%

:statloop
if %statcount% == 0 goto syssave
cls
echo dotOS util. versions available: %statcount%
echo 1 - Decoder Level VERS. %decoderlvl%.0
echo Your decoder is first thing that you (as a Hyper-User) use to combat viruses!
echo 2 - Encryptor Level VERS. %encryptorlvl%.0
echo Your encryptor is the second that that you (as a Hyper-User) use to combat viruses more powerfully!
echo 3 - dotOS VERS. %dotOSHP%.0
echo dotOS is your current system! Keep this up to date to prevent total system destruction!
echo 4 - System Code VERS. %syscodelvl%.0
echo This is the buffer on the dotOS code! Keep this up to date to prevent total system destruction!
echo. 
set /p statchoices=Please select your choice:: 
if %statchoices% == 1 (
set /a decoderlvl=%decoderlvl%+1
set /a statcount=%statcount%-1
goto statloop
)
if %statchoices% == 2 (
set /a encryptorlvl=%encryptorlvl%+1
set /a statcount=%statcount%-1
goto statloop
)
if %statchoices% == 3 (
set /a dotOSHP=%dotOSHP%+5
set /a statcount=%statcount%-1
goto statloop
)
if %statchoices% == 4 (
set /a syscodelvl=%syscodelvl%+1
set /a statcount=%statcount%-1
goto statloop 
)
goto statloop 

:: For a hyper-compact statloop system, I'm rather proud of the outcome. 

:syssave
cls
if not exist SAVE md SAVE
echo Thank you for your inputs. Right now, we are going to be
echo saving your hyper-user data into our database!
echo This will only take a brief moment. 
echo. 
(
echo %decoderlvl%
echo %encryptorlvl%
echo %dotOSHP%
echo %syscodelvl%
echo   
echo Hey there. I see you're poking around through my save. 
echo I guess that's okay, but I'd really rather you play it
echo legitimately. Sure, you can superpower your dotOS, but where's the fun?
echo I get it though. So I'll be nice enough to give you the
echo variables. 
echo    
echo decoderlvl 
echo encryptorlvl
echo dotOSHP
echo syscodelvl
)>SAVE\%hyperu%.ini
(
echo Your save file exists! 
)>SAVE\CONFIRM%hyperu%.txt
echo. 
echo Thank you for your brief patience. 
echo Your HYPER-u details were saved accordingly. 
echo. 
ping localhost -n 3 >nul 
cls
ping localhost -n 5 >nul 
set musicneeded=2
start player.bat
call CALLUPONS\inbound.bat 
echo HyperUser %hyperu% is now in direct combat with virus!
echo.
echo We are now transmitting you to the Hyper-U Virus Defender...
echo This will only take a brief moment...
ping localhost -n 4 >nul
set firstTime=1
call BTLDATA\virdefender.bat
cls
echo check1 success
pause
cls
set firstTime=0
call BTLDATA\virdefender.bat
echo check2 success 
pause 
cls 
echo Oh hey, it works. 
echo Also. Here, i'll exit for you. 
ping localhost -n 3 >nul
exit 



:loadsavestate
cls
echo placeholder
pause
exit 

:fancyShutdown
cls
:fancyShutdown2
echo Your terminal is OK to shutdown.
pause >nul 
goto fancyShutdown2


















