#!bin/bash

# This is ONLY a basic test. 
# It's not meant to be taken as "official versioning"
# But it definitely is a first for the first branch of the .SH line of DVGR.

# I do however, have plans to complete DVGR BATCH first. If I ever feel 
# like I need to make a Linux/UNIX version, this is where I'll be calling from.

# So in essence, this is version is
version="1.0"

echo -ne "\e]0;dotVirus Grand Return $version\a"
clear
sleep 3 
echo If you have not read the source code, I highly recommend it.
echo Otherwise you may have a couple issues with running this version of DVGR.
echo 
echo I am still novice with BASH programming, so forgive me. 
sleep 5
clear
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo A game written originally by Shubshub
echo 
echo 
sleep 5
clear
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo Reprogrammed and Reimagined by OrbisDev
echo 
echo 
sleep 5 
clear 
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo Welcome to dotVirus: Grand Return LinuxVER:$version
echo 
echo 
sleep 5 

clear
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo 
echo Welcome to dotVirus: Grand Return LinuxVER:$version
echo 
echo Please enter your inputs on the digital terminal
echo 
echo "1) Boot dvgr.sys"
echo "2) Load savestate"
echo "3) Update terminal"
echo "4) Close Terminal"
read bootLoad
if $bootLoad = 1 then
	clear
	echo This demo ends here.
	echo There will be more eventually.
	sleep 7
	exit 0
fi
if $bootload != 1 then
	clear
	echo There is nothing else here. Sorry.
	echo End of DVGR Linux demo!
	sleep 7
	exit 0
fi

exit 64
